﻿#pragma checksum "..\..\CreatePlayer_Window.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "FF88D4ED0DE7EE39636BDCB34FE1D4CC8BD6BC1C9C70080381493064167ECBD3"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using TP4;


namespace TP4 {
    
    
    /// <summary>
    /// CreatePlayer_Window
    /// </summary>
    public partial class CreatePlayer_Window : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\CreatePlayer_Window.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbPrenom;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\CreatePlayer_Window.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbNom;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\CreatePlayer_Window.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbAlias;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\CreatePlayer_Window.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblPrenom;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\CreatePlayer_Window.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblNom;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\CreatePlayer_Window.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblAlias;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\CreatePlayer_Window.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btValider;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\CreatePlayer_Window.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btAnnuler;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/TP4;component/createplayer_window.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\CreatePlayer_Window.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.tbPrenom = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.tbNom = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.tbAlias = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.lblPrenom = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.lblNom = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.lblAlias = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.btValider = ((System.Windows.Controls.Button)(target));
            
            #line 16 "..\..\CreatePlayer_Window.xaml"
            this.btValider.Click += new System.Windows.RoutedEventHandler(this.btValider_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btAnnuler = ((System.Windows.Controls.Button)(target));
            
            #line 17 "..\..\CreatePlayer_Window.xaml"
            this.btAnnuler.Click += new System.Windows.RoutedEventHandler(this.btAnnuler_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

