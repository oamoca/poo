﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SpaceInvadersArmory;

namespace TP4
{
    /// <summary>
    /// Logique d'interaction pour CreateArme_Window.xaml
    /// </summary>
    public partial class CreateArme_Window : Window
    {
        public CreateArme_Window()
        {
            InitializeComponent();
        }

        public string nom
        {
            get { return tbNom.Text; }
        }
        public int type
        {
            get { return cbType.SelectedIndex; }
        }
        public int minDamage
        {
            get { return int.Parse(tbMinDamage.Text); }
        }

        public int maxDamage
        {
            get { return int.Parse(tbMaxDamage.Text); }
        }

        private void btAnnuler_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btValider_Click(object sender, RoutedEventArgs e)
        {
            if (tbNom.Text != "" || tbMinDamage.Text != "" || tbMaxDamage.Text != "" || cbType.SelectedItem != null)
                this.DialogResult = true;
            else
                MessageBox.Show("Veuillez saisir tous les informations de l'arme !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Warning);

        }
    }
}
