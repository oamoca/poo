﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TP4
{
    /// <summary>
    /// Logique d'interaction pour CreatePlayer_Window.xaml
    /// </summary>
    public partial class CreatePlayer_Window : Window
    {
        public CreatePlayer_Window()
        {
            InitializeComponent();
        }

        public string prenom
        {
            get { return tbPrenom.Text; }
        }
        public string nom
        {
            get { return tbNom.Text; }
        }
        public string alias
        {
            get { return tbAlias.Text; }
        }

        private void btAnnuler_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btValider_Click(object sender, RoutedEventArgs e)
        {
            if(tbPrenom.Text != "" || tbNom.Text != "" || tbAlias.Text != "")
                this.DialogResult = true;
            else
                MessageBox.Show("Veuillez saisir tous les informations du joueur !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Warning);
        }
    }
}
