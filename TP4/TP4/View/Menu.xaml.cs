﻿using System.Windows;
using System;
using System.Collections.Generic;
using Models;
using SpaceInvadersArmory;
using Microsoft.Win32;
using System.IO;
using System.Windows.Media.Imaging;
using TP4.ViewModel;

namespace TP4
{
    /// <summary>
    /// Logique d'interaction pour Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        public PlayerViewModel playerViewModel;

        public Menu()
        {
            InitializeComponent();

            playerViewModel = new PlayerViewModel();
            dgPlayer.DataContext = playerViewModel;
            Player me = new Player("Okkes", "Amoca", "LERIWAY");
            me.BattleShip = new Models.SpaceShips.Dart(false);
            playerViewModel.Players.Add(me);
            playerViewModel.Players.Add(new Player("Victor", "Larmet", "Spartan"));
        }

        private void btAddPlayer_Click(object sender, RoutedEventArgs e)
        {
            CreatePlayer_Window createPlayer_win = new CreatePlayer_Window();

            if ((bool)createPlayer_win.ShowDialog())
            {
                string prenom = createPlayer_win.prenom;
                string nom = createPlayer_win.nom;
                string alias = createPlayer_win.alias;

                Player player = new Player(prenom, nom, alias);
                playerViewModel.Players.Add(player);
            }
        }

        private void btRemovePlayer_Click(object sender, RoutedEventArgs e)
        {
            if (dgPlayer.SelectedItem != null)
                playerViewModel.Players.Remove(playerViewModel.SelectedPlayer);
            else
                MessageBox.Show("Veuillez choisir un Joueur à supprimer !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void dgPlayer_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (dgPlayer.SelectedItem != null)
            {
                btRemovePlayer.IsEnabled = true;
                btAddArme.IsEnabled = true;
                btDeleteArme.IsEnabled = true;
                btInsertImage.IsEnabled = true;
            }
            else
            {
                btAddArme.IsEnabled = false;
                btDeleteArme.IsEnabled = false;
                btInsertImage.IsEnabled = false;

                btRemovePlayer.IsEnabled = false;
            }
        }

        private void btDeleteArme_Click(object sender, RoutedEventArgs e)
        {
            if (dgListeArmes.SelectedItem != null)
            {
                playerViewModel.SelectedPlayer.BattleShip.Weapons.Remove((SpaceInvadersArmory.Weapon)dgListeArmes.SelectedItem);
            }
            else
                MessageBox.Show("Veuillez choisir une arme à supprimer !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void btInsertArmory_Click(object sender, RoutedEventArgs e)
        {
            BlueprintImporter.ImportBlueprintFromText();
        }

        // Retourne le type de l'arme
        private EWeaponType TypeArme(int curretnArme)
        {
            EWeaponType typeArme;
            if (curretnArme == 0)
                typeArme = EWeaponType.Direct;
            else if (curretnArme == 1)
                typeArme = EWeaponType.Explosive;
            else
                typeArme = EWeaponType.Guided;

            return typeArme;
        }

        private void btCreateArme_Click(object sender, RoutedEventArgs e)
        {
            CreateArme_Window createWeapon_win = new CreateArme_Window();

            if ((bool)createWeapon_win.ShowDialog())
            {
                string nom = createWeapon_win.nom;
                int minDamage = createWeapon_win.minDamage;
                int maxDamage = createWeapon_win.maxDamage;
                int type = createWeapon_win.type;

                EWeaponType typeArme = TypeArme(type);

                WeaponBlueprint weaponBlueprint = Armory.CreatBlueprint(nom, typeArme, minDamage, maxDamage);
                Weapon weapon = Armory.CreatWeapon(weaponBlueprint);
            }
        }

        private void btAddArme_Click(object sender, RoutedEventArgs e)
        {
            if (playerViewModel.SelectedPlayer.BattleShip.Name == "ViperMKII" && dgListeArmes.Items.Count == 3)
                MessageBox.Show("Ce vaisseau peut avoir que 3 armes, veuillez en supprimer un !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Warning);
            else
            {
                AddArme_Window addArme_win = new AddArme_Window();

                if ((bool)addArme_win.ShowDialog())
                {
                    WeaponBlueprint weaponBlueprint = addArme_win.arme;
                    Weapon weapon = Armory.CreatWeapon(weaponBlueprint);
                    playerViewModel.SelectedPlayer.BattleShip.AddWeapon(weapon);
                    //dgListeArmes.Items.Add(weapon);
                }
            }

        }

        private void btInsertImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select a picture";
            openFileDialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (openFileDialog.ShowDialog() == true)
            {
                imgVaisseau.Source = new BitmapImage(new Uri(openFileDialog.FileName));
                playerViewModel.SelectedPlayer.BattleShip.imgVaisseau = new BitmapImage(new Uri(openFileDialog.FileName));
            }
        }
    }
}
