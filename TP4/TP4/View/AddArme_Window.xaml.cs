﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SpaceInvadersArmory;

namespace TP4
{
    /// <summary>
    /// Logique d'interaction pour AddArme_Window.xaml
    /// </summary>
    public partial class AddArme_Window : Window
    {
        public WeaponBlueprint arme
        {
            get { return (WeaponBlueprint)cbArmory.SelectedItem; }
        }

        public AddArme_Window()
        {
            InitializeComponent();

            foreach(var arme in Armory.Blueprints)
            {
                cbArmory.Items.Add(arme);
            }
        }

        private void btValider_Click(object sender, RoutedEventArgs e)
        {
            if(cbArmory.SelectedItem!=null)
                this.DialogResult = true;
            else
                MessageBox.Show("Veuillez choisir une arme à ajouter !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Warning);
        }
    }
}
