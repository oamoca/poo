﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Extends
{
    public static class CollectionExtentions
    {
        /// <summary>
        /// Methode d'extention permetant de cloner une liste d'objet
        /// </summary>
        /// <typeparam name="T">le type d'objet contenue dans la liste</typeparam>
        /// <param name="listToClone">La liste clonée</param>
        /// <remarks>T doit implémanter l'inteface ICloneable</remarks>
        /// <returns>Le clone</returns>
        public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }
        public static T[] SubArray<T>(this T[] data, int startIndex)
        {
            return data.SubArray(startIndex, data.Length - startIndex);
        }
        public static T[] SubArray<T>(this T[] data, int startIndex, int length)
        {
            if (data == null) return null;
            if (length == 0) return null;
            if (startIndex > data.Length) throw new Exception("start index out of data limit");
            if (length > data.Length - startIndex) return data.SubArray(startIndex);
            T[] result = new T[length];
            Array.Copy(data, startIndex, result, 0, length);
            return result;
        }
    }
}
