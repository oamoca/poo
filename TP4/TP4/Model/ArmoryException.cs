﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceInvadersArmory
{
    public class ArmoryException : Exception
    {
        public ArmoryException(WeaponBlueprint bp):base($"This weapon blueprint [{bp.Name}] does not come from the armory!") { }
        public ArmoryException(Weapon w) : base($"This weapon [{w.Name}] does not come from the armory!") { }
    }
}
