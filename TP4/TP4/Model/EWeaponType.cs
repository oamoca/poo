﻿using System.ComponentModel;

namespace SpaceInvadersArmory
{
    public enum EWeaponType
    {
        [Description("Direct weapon")]
        Direct = 1,
        [Description("Explosive weapon")]
        Explosive = 2,
        [Description("Guided weapon")]
        Guided = 3
    }
}
