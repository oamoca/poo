﻿using Extends;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;

namespace SpaceInvadersArmory
{
    public static class BlueprintImporter
    {
        private static Dictionary<string, int> GetWordFrequency(string filePath, int minWordSize, List<string> blackList)
        {
            if (string.IsNullOrWhiteSpace(filePath)) { throw new Exception("filePath is empty !"); }
            if (!File.Exists(filePath)) { throw new Exception($"file : [{filePath}] doesnt exist !"); }
            if (Path.GetExtension(filePath).ToLower() != ".txt") { throw new Exception($"file : [{filePath}] isnt a text file !"); }
            if (blackList == null) { blackList = new List<string>(); }
            return File.ReadLines(filePath)
                .SelectMany(x => x.Split())                             //on divise le texte par mot
                .Where(x => WordValidation(x, minWordSize, blackList))  //on prend que les mots valide
                .GroupBy(x => x)                                        //on groupe par mot
                .ToDictionary(x => x.Key.DeleteSpecialChar().Proper(), x => x.Count());               //on crée un dictionnaire avec le mot comme clé et son nombre d'itération comme frequence
        }

        private static bool WordValidation(string word, int minWordSize, List<string> blackList)
        {
             return word.Length >= minWordSize && !blackList.Contains(word);
        }

        public static void ImportBlueprintFromText()//string file, int minWordSize, List<string> blackList)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Texte Files (*.txt)|*.txt|All files (*.*)|*.*";
            string fiche;
            string[] lsArmory;

            if (openFileDialog.ShowDialog() == true)
            {
                fiche = File.ReadAllText(openFileDialog.FileName);
                //txtEditor.Text = fiche;
                lsArmory = fiche.Split('\n');
                //txtEditor.Text += "\n"+lsNDA[0];

                string lsName;
                int lsType;
                int lsMinDamage;
                int lsMaxDamage;

                foreach (string elm in lsArmory)
                {
                    // récup les infos de l'arme, format : nom | type | minDamage | maxDamage
                    lsArmory = elm.Split(' ');

                    if (lsArmory.Count() != 4)
                    {
                        MessageBox.Show("Le format du fichier ne correspond pas ! \nFormatage requis : [nom type minDamage maxDamage] \nExemple de formatage : https://gitlab.com/oamoca/poo/-/blob/main/Armory.txt");
                        break;
                    }

                    lsName = lsArmory[0];
                    lsType = int.Parse(lsArmory[1]);
                    lsMinDamage = int.Parse(lsArmory[2]);
                    lsMaxDamage = int.Parse(lsArmory[3].Replace("\n", "").Replace("\r", ""));

                    EWeaponType typeArme = TypeArme(lsType);

                    // Ajoute dans l'armurerie
                    WeaponBlueprint weaponBlueprint = Armory.CreatBlueprint(lsName, typeArme, lsMinDamage, lsMaxDamage);
                    //Weapon weapon = Armory.CreatWeapon(weaponBlueprint);
                }
            }


/*            Random r = new Random();
            foreach (var item in GetWordFrequency(file, minWordSize, blackList))
            {
                Armory.CreatBlueprint(item.Key, (EWeaponType)r.Next(0, 3), Math.Min(item.Key.Length, item.Value), Math.Max(item.Key.Length, item.Value));
            }*/
        }

        // Retourne le type de l'arme
        private static EWeaponType TypeArme(int curretnArme)
        {
            EWeaponType typeArme;
            if (curretnArme == 0)
                typeArme = EWeaponType.Direct;
            else if (curretnArme == 1)
                typeArme = EWeaponType.Explosive;
            else
                typeArme = EWeaponType.Guided;

            return typeArme;
        }
    }
}
