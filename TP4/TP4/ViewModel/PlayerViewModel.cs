﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace TP4.ViewModel
{
    public class PlayerViewModel : INotifyPropertyChanged
    {
        ObservableCollection<Player> _players = new ObservableCollection<Player>();
        Player _player;

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<Player> Players
        {
            get
            {
                return _players;
            }

            set
            {
                if (_players == value)
                {
                    return;
                }

                _players = value;
            }
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public Player SelectedPlayer
        {
            get
            {
                return _player;
            }

            set
            {
                if (_player == value)
                {
                    return;
                }

                _player = value;
            }
        }
    }
}
